public class M {
    private B b;

    public M(){
    }

    public M(B bCopy){
        this.b=bCopy;
    }
}

interface interface1{
    void metA();
}

class A implements interface1{
    private M m;
    public A(){
        this.m = new M();
    }

    public void metA(){
    }
}

class B{
    public void metB(){
    }
}

class L{
    private long t;
    private M m;

    public void f(){
    }
}

class X{
    public void i(L l){
    }
}