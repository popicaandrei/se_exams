import java.time.Instant;

public class ThreadsApp extends Thread{

    synchronized public void run(){
        int i=10;
        while(i>0) {
            try {
                System.out.println(Thread.currentThread().getName() +" - "+ Instant.now());
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            i--;
        }
    }

    public static void main(String[] args) throws InterruptedException {

        ThreadsApp t1 = new ThreadsApp();

        Thread[] threads = new Thread[4];
        for(int i=0;i<4;i++)
        {
            threads[i]=new Thread(t1);
            threads[i].setName("NewThread"+(i+1));
            threads[i].start();
            threads[i].join(); //pentru a termina fiecare fir de executie si a le avea in ordine
        }

//        SAU ALTA VARIANTA
//        Thread NewThread1 = new Thread(t1);
//        NewThread1.setName("NewThread1");
//
//        Thread NewThread2 = new Thread(t1);
//        NewThread2.setName("NewThread2");
//
//        Thread NewThread3 = new Thread(t1);
//        NewThread3.setName("NewThread3");
//
//        Thread NewThread4 = new Thread(t1);
//        NewThread4.setName("NewThread4");
//
//
//        NewThread1.start();
//        NewThread2.start();
//        NewThread3.start();
//        NewThread4.start();
    }

}
